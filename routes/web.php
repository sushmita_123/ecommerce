<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Backend\TagController;
use App\Http\Controllers\Backend\CategoryController;
use App\Http\Controllers\Backend\SubcategoryController;
use App\Http\Controllers\Backend\RoleController;
use App\Http\Controllers\Backend\UserController;
use App\Http\Controllers\Backend\ModuleController;
use App\Http\Controllers\Backend\ProductController;
use App\Http\Controllers\Backend\AttributeController;
use App\Http\Controllers\Backend\PermissionController;
use App\Http\Controllers\Backend\SettingController;
use App\Http\Controllers\Backend\BrandController;
    use App\Http\Controllers\Backend\SliderController;

Route::get('/', [\App\Http\Controllers\Frontend\FrontController::class, 'index'])->name('frontend.front.index');

Auth::routes();

Route::get('/home', [App\Http\Controllers\DashboardController::class, 'index'])->name('home');
Route::get('/table', [App\Http\Controllers\DashboardController::class, 'table'])->name('table');
Route::get('/form', [App\Http\Controllers\DashboardController::class, 'form'])->name('form');

Route::prefix('backend')->name('backend.')->middleware('auth')->group(function () {
    Route::get('/tag/trash', [TagController::class, 'trash'])->name('tag.trash');
    Route::get('/tag/restore/{id}', [TagController::class, 'restore'])->name('tag.restore');
    Route::delete('/tag/force-delete/{id}', [TagController::class, 'forceDelete'])->name('tag.force_delete');

    Route::resource('tag', TagController::class);
});

Route::prefix('backend')->name('backend.')->middleware(['auth','permission'])->group(function () {
    Route::get('/category/trash', [CategoryController::class, 'trash'])->name('category.trash');
    Route::get('/category/restore/{id}', [CategoryController::class, 'restore'])->name('category.restore');
    Route::delete('/category/force-delete/{id}', [CategoryController::class, 'forceDelete'])->name('category.force_delete');
    Route::post('/category/get_subcategory', [CategoryController::class, 'getSubcategory'])->name('category.get_subcategory');

    Route::resource('category', CategoryController::class);
});

Route::prefix('backend')->name('backend.')->middleware('auth')->group(function () {
    Route::get('/subcategory/trash', [SubcategoryController::class, 'trash'])->name('subcategory.trash');
    Route::get('/subcategory/restore/{id}', [SubcategoryController::class, 'restore'])->name('subcategory.restore');
    Route::delete('/subcategory/force-delete/{id}', [SubcategoryController::class, 'forceDelete'])->name('subcategory.force_delete');

    Route::resource('subcategory', SubcategoryController::class);
});


Route::prefix('backend')->name('backend.')->middleware(['auth','permission'])->group(function () {
    Route::get('/role/trash', [RoleController::class, 'trash'])->name('role.trash');
    Route::get('/role/restore/{id}', [RoleController::class, 'restore'])->name('role.restore');
    Route::delete('/role/force-delete/{id}', [RoleController::class, 'forceDelete'])->name('role.force_delete');
    Route::get('/role/assign_permission_form/{id}', [RoleController::class, 'assignPermissionForm'])->name('role.assign_permission_form');
    Route::post('/role/assign_permission/{id}', [RoleController::class, 'assignPermission'])->name('role.assign_permission');

    Route::resource('role', RoleController::class);
});


Route::prefix('backend')->name('backend.')->middleware('auth')->group(function () {
    Route::get('/user/trash', [UserController::class, 'trash'])->name('user.trash');
    Route::get('/user/restore/{id}', [UserController::class, 'restore'])->name('user.restore');
    Route::delete('/user/force-delete/{id}', [UserController::class, 'forceDelete'])->name('user.force_delete');

    Route::resource('user', UserController::class);
});


Route::prefix('backend')->name('backend.')->middleware('auth')->group(function () {
    Route::get('/module/trash', [ModuleController::class, 'trash'])->name('module.trash');
    Route::get('/module/restore/{id}', [ModuleController::class, 'restore'])->name('module.restore');
    Route::delete('/module/force-delete/{id}', [ModuleController::class, 'forceDelete'])->name('module.force_delete');
    Route::resource('module', ModuleController::class);
});

Route::prefix('backend')->name('backend.')->middleware(['auth','permission'])->group(function () {
    Route::get('/product/trash', [ProductController::class, 'trash'])->name('product.trash');
    Route::get('/product/restore/{id}', [ProductController::class, 'restore'])->name('product.restore');
    Route::delete('/product/force-delete/{id}', [ProductController::class, 'forceDelete'])->name('product.force_delete');
    Route::resource('product', ProductController::class);
});


Route::prefix('backend')->name('backend.')->middleware('auth')->group(function () {
    Route::get('/attribute/trash', [AttributeController::class, 'trash'])->name('attribute.trash');
    Route::get('/attribute/restore/{id}', [AttributeController::class, 'restore'])->name('attribute.restore');
    Route::delete('/attribute/force-delete/{id}', [AttributeController::class, 'forceDelete'])->name('attribute.force_delete');
    Route::resource('attribute', AttributeController::class);
});

Route::prefix('backend')->name('backend.')->middleware('auth')->group(function () {
    Route::get('/permission/trash', [PermissionController::class, 'trash'])->name('permission.trash');
    Route::get('/permission/restore/{id}', [PermissionController::class, 'restore'])->name('permission.restore');
    Route::delete('/permission/force-delete/{id}', [PermissionController::class, 'forceDelete'])->name('permission.force_delete');
    Route::resource('permission', PermissionController::class);
});

Route::prefix('backend')->name('backend.')->middleware('auth')->group(function () {
    Route::get('/setting/trash', [SettingController::class, 'trash'])->name('setting.trash');
    Route::get('/setting/restore/{id}', [SettingController::class, 'restore'])->name('setting.restore');
    Route::delete('/setting/force-delete/{id}', [SettingController::class, 'forceDelete'])->name('setting.force_delete');
    Route::resource('setting', SettingController::class);
});

Route::prefix('backend')->name('backend.')->middleware('auth')->group(function () {
    Route::get('/brand/trash', [BrandController::class, 'trash'])->name('brand.trash');
    Route::get('/brand/restore/{id}', [BrandController::class, 'restore'])->name('brand.restore');
    Route::delete('/brand/force-delete/{id}', [BrandController::class, 'forceDelete'])->name('brand.force_delete');
    Route::resource('brand', BrandController::class);
});


Route::prefix('backend')->name('backend.')->middleware('auth')->group(function () {
    Route::get('/slider/trash', [SliderController::class, 'trash'])->name('slider.trash');
    Route::get('/slider/restore/{id}', [SliderController::class, 'restore'])->name('slider.restore');
    Route::delete('/slider/force-delete/{id}', [SliderController::class, 'forceDelete'])->name('slider.force_delete');
    Route::resource('slider', SliderController::class);
});
