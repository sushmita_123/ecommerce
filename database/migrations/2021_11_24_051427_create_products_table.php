<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('slug')->unique();
            $table->integer('code')->unique();
            $table->text('short_description');
            $table->text('description');
            $table->float('price');
            $table->integer('quantity');
            $table->integer('stock');
            $table->boolean('feature_key')->default('0');
            $table->boolean('flash_key')->default('0');
            $table->string('meta_keyword')->nullable('n');
            $table->text('meta_title')->nullable('n');
            $table->text('meta_description')->nullable('n');
            $table->softDeletes();
            $table->unsignedBigInteger('category_id');
            $table->foreign('category_id')->references('id')->on('categories');
            $table->unsignedBigInteger('subcategory_id');
            $table->foreign('subcategory_id')->references('id')->on('sub_categories');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
