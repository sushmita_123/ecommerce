<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
           'id' => 1,
            'name' => 'Admin Role',
            'status' => 1,
            'created_by' => 1
        ]);

        DB::table('roles')->insert([
            'id' => 2,
            'name' => 'Editor Role',
            'status' => 1,
            'created_by' => 1
        ]);

        $user = \App\Models\User::find(1);
        $user->update([
           'role_id' => 1
        ]);

        $user = \App\Models\User::find(2);
        $user->update([
            'role_id' => 2
        ]);


    }
}
