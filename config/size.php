<?php
    return  [
             'category' =>    [
                    [
                        'width' => 85,
                        'height' => 100
                    ],
                    [
                        'width' => 500,
                        'height' => 600
                    ],
                    [
                        'width' => 1000,
                        'height' => 1200
                    ]
                ],
            'subcategory' =>    [
                [
                    'width' => 200,
                    'height' => 100
                ],
                [
                    'width' => 500,
                    'height' => 600
                ],
                [
                    'width' => 1000,
                    'height' => 1200
                ]
            ],
            'product' =>    [
                [
                    'width' => 250,
                    'height' => 250
                ],
                [
                    'width' => 500,
                    'height' => 600
                ],
                [
                    'width' => 1000,
                    'height' => 1200
                ]
            ],
            'customer' =>    [
                [
                    'width' => 200,
                    'height' => 100
                ],
                [
                    'width' => 500,
                    'height' => 600
                ],
                [
                    'width' => 1000,
                    'height' => 1200
                ]
            ],
        'brand'=>[
            [
                'width' => 500,
                'height' => 600
            ],
            [
                'width' => 1000,
                'height' => 1200
            ]
        ],
        'slider' =>    [
            [
                'width' => 200,
                'height' => 100
            ],
            [
                'width' => 500,
                'height' => 600
            ],
            [
                'width' => 1000,
                'height' => 1200
            ]
        ],


        ];
