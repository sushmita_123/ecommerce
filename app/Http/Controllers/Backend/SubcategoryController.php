<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\SubCategoryRequest;
use App\Models\Category;
use App\Models\Subcategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class SubcategoryController extends BackendBaseController
{
    protected $panel = 'Subcategory';
    protected  $base_path = 'backend.subcategory.';
    protected  $base_route = 'backend.subcategory.';

    public function  __construct(){
        $this->model = new Subcategory();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['records'] = $this->model->all();
        return view($this->__loadDataToView($this->base_path . 'index'),compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
            $data['categories'] = Category::pluck('name','id');
        return view( $this->__loadDataToView($this->base_path . 'create'),compact('data'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SubcategoryRequest $request)
    {
        $request->request->add(['created_at' => Auth::user()->id]);

        if ($request->hasFile('image_file'))
        {
            $image = $request->file('image_file');
            //image storage
            $image_name = uniqid() . '_' . $image->getClientOriginalName();
            $image->move('assets/images/subcategory',$image_name);
            $request->request->add(['image' => $image_name]);
//image_resize
            $sizes = config('size.subcategory');
            $imager = Image::make('assets/images/subcategory/'.$image_name);

            foreach ($sizes as $size) {
                $imager->resize ($size['width'],$size['height']);
                $imager->save('assets/images/subcategory/'. $size['width'] . '_' . $size['height'] . '_' .$image_name);
            }
        }
        $sub_category = $this->model->create($request->all());
        if ($sub_category){
            $request->session()->flash('success',$this->panel . ' created successfully');
        } else {
            $request->session()->flash('error',$this->panel . ' creation failed');
        }
        return redirect()->route($this->base_route . 'index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['record'] = $this->model->findOrFail($id);
        return  view($this->__loadDataToView($this->base_path . 'show'),compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['categories'] = Category::pluck('name','id');
        $data['record'] = $this->model->findOrFail($id);
        return  view($this->__loadDataToView($this->base_path . 'edit'),compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SubcategoryRequest $request, $id)
    {
        $data['record'] = $this->model->findOrFail($id);

        $request->request->add(['updated_at' => Auth::user()->id]);

        if ($data['record']->update($request->all())){
            $request->session()->flash('success',$this->panel . ' updated successfully');
        } else {
            $request->session()->flash('error',$this->panel . ' update failed');
        }
        return redirect()->route($this->base_route . 'index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data['record'] = $this->model->findOrFail($id);
        if ($data['record']->delete()){
            request()->session()->flash('success',$this->panel . ' deleted successfully');
        } else {
            request()->session()->flash('error',$this->panel . ' delete failed');
        }
        return redirect()->route($this->base_route . 'index');
    }
    public function trash()
    {
        $data['records'] = $this->model->onlyTrashed()->get();
        return view($this->__loadDataToView($this->base_path . 'trash'),compact('data'));
    }

    public function restore($id)
    {
        $data['record'] = $this->model->onlyTrashed()->where('id',$id)->restore();
        request()->session()->flash('success',$this->panel . ' restored');

        return redirect()->route($this->base_route . 'trash');
    }

    public function forceDelete(Request  $request,$id)
    {
        $data['record'] = $this->model->onlyTrashed()->where('id',$id)->forceDelete();
        request()->session()->flash('success',$this->panel . ' deleted successfully');
        return redirect()->route($this->base_route . 'trash');
    }




}
