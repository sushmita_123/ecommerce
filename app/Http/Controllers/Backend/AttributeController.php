<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\AttributeRequest;
use App\Models\Tag;
use App\Models\Attribute;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AttributeController extends BackendBaseController
{
    protected $panel = 'Attribute';
    protected  $base_path = 'backend.attribute.';
    protected  $base_route = 'backend.attribute.';

    public function  __construct(){
        $this->model = new Attribute();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['records'] = $this->model->all();
        return view($this->__loadDataToView($this->base_path . 'index'),compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['products'] = Product::pluck('name','id');
        return view( $this->__loadDataToView($this->base_path . 'create'),compact('data'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AttributeRequest $request)
    {
        $request->request->add(['created_at' => Auth::user()->id]);
        $attribute= $this->model->create($request->all());
        if ($attribute){
            $request->session()->flash('success',$this->panel . ' created successfully');
        } else {
            $request->session()->flash('error',$this->panel . ' creation failed');
        }
        return redirect()->route($this->base_route . 'index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['record'] = $this->model->findOrFail($id);
        return  view($this->__loadDataToView($this->base_path . 'show'),compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['tags'] = Tag::pluck('name','id');
        $data['products'] = Product::pluck('name','id');
        $data['record'] = $this->model->findOrFail($id);
        return  view($this->__loadDataToView($this->base_path . 'edit'),compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AttributeRequest $request, $id)
    {
        $data['record'] = $this->model->findOrFail($id);

        $request->request->add(['updated_at' => Auth::user()->id]);

        if ($data['record']->update($request->all())){
            $request->session()->flash('success',$this->panel . ' updated successfully');
        } else {
            $request->session()->flash('error',$this->panel . ' update failed');
        }
        return redirect()->route($this->base_route . 'index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data['record'] = $this->model->findOrFail($id);
        if ($data['record']->delete()){
            request()->session()->flash('success',$this->panel . ' deleted successfully');
        } else {
            request()->session()->flash('error',$this->panel . ' delete failed');
        }
        return redirect()->route($this->base_route . 'index');
    }
    public function trash()
    {
        $data['records'] = $this->model->onlyTrashed()->get();
        return view($this->__loadDataToView($this->base_path . 'trash'),compact('data'));
    }

    public function restore($id)
    {
        $data['record'] = $this->model->onlyTrashed()->where('id',$id)->restore();
        request()->session()->flash('success',$this->panel . ' restored');

        return redirect()->route($this->base_route . 'trash');
    }

    public function forceDelete(Request  $request,$id)
    {
        $data['record'] = $this->model->onlyTrashed()->where('id',$id)->forceDelete();
        request()->session()->flash('success',$this->panel . ' deleted successfully');
        return redirect()->route($this->base_route . 'trash');
    }




}
