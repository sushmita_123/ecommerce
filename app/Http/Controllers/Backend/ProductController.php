<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Models\Attribute;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductAttribute;
use App\Models\ProductImage;
use App\Models\Subcategory;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class ProductController extends BackendBaseController
{
    protected $panel = 'Product';
    protected  $base_path = 'backend.product.';
    protected  $base_route = 'backend.product.';

    public function  __construct(){
        $this->model = new Product();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['records'] = $this->model->all();
        return view($this->__loadDataToView($this->base_path . 'index'),compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['categories'] = Category::pluck('name','id');
        $data['sub_categories'] = Subcategory::pluck('name','id');
        $data['tags'] = Tag::pluck('name','id');
        $data['attributes'] = Attribute::pluck('name','id');
        $data['assigned_tag'] = [];

        return view( $this->__loadDataToView($this->base_path . 'create'),compact('data'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        $request->request->add(['created_at' => Auth::user()->id]);
        $request->request->add(['stock' => $request->quantity]);

        $product= $this->model->create($request->all());

        if ($product){
            $product->tags()->attach($request->tag_id);
            //process to save attribute

            $attribute_ids = $request->attribute_id;
            $attribute_values = $request->attribute_value;

            $attArray['product_id'] = $product->id;

            for ($i = 0;$i < count($attribute_values);$i++){
                if ($attribute_values[$i] != null && $attribute_ids[$i] != null ){
                    //save to product attribute
                    $attArray['attribute_id'] = $attribute_ids[$i];
                    $attArray['value'] = $attribute_values[$i];
                    ProductAttribute::create($attArray);

                }
            }

            if ($request->hasFile('image_file'))
            {
                $imgArray['product_id'] = $product->id;
                $imgArray['status'] = 1;

                $images = $request->file('image_file');
                $image_titles = $request->input('image_title');

                if(count($images) > 0){
                    for ($i=0;$i<count($images);$i++){
                        $name = uniqid() . '_' . $images[$i]->getClientOriginalName();
                        $images[$i]->move('assets/images/product',$name);

                        $imgArray['title'] = $image_titles[$i];
                        $imgArray['name'] = $name;
                        ProductImage::create($imgArray);

                        $sizes = config('size.product');
                        $imager = Image::make('assets/images/product/'.$name);

                        foreach ($sizes as $size) {
                            $imager->resize ($size['width'],$size['height']);
                            $imager->save('assets/images/product/'. $size['width'] . '_' . $size['height'] . '_' .$name);
                        }

                    }

                }
            }


            $request->session()->flash('success',$this->panel . ' created successfully');
        } else {
            $request->session()->flash('error',$this->panel . ' creation failed');
        }
        return redirect()->route($this->base_route . 'index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['record'] = $this->model->findOrFail($id);
        return  view($this->__loadDataToView($this->base_path . 'show'),compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['categories'] = Category::pluck('name','id');
        $data['sub_categories'] = Subcategory::pluck('name','id');
        $data['tags'] = Tag::pluck('name','id');
        $data['attributes'] = Attribute::pluck('name','id');
        $data['record'] = $this->model->findOrFail($id);

        $tags = $data['record']->tags()->get();
        $data['assigned_tag']  = [];
        foreach ($tags  as $tag){
            $data['assigned_tag'][] = $tag->id;
        }

       return  view($this->__loadDataToView($this->base_path . 'edit'),compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, $id)
    {
        $data['record'] = $this->model->findOrFail($id);

        $request->request->add(['updated_at' => Auth::user()->id]);


        $product = $data['record']->update($request->all());
        if ($product) {

            $data['record']->tags()->sync($request->tag_id);
            //process to save and update attribute

            $attribute_ids = $request->attribute_id;
            $attribute_values = $request->attribute_value;

            $attArray['product_id'] = $data['record']->id;

            for ($i = 0; $i < count($attribute_values); $i++) {
                if ($attribute_values[$i] != null && $attribute_ids[$i] != null) {
                    //save to product attribute
                    $attArray['attribute_id'] = $attribute_ids[$i];
                    $attArray['value'] = $attribute_values[$i];
                    $old_attribute = ProductAttribute::where('product_id', $data['record']->id)->where('attribute_id', $attribute_ids[$i])->first();
                    if ($old_attribute) {
                        $old_attribute->update($attArray);
                    } else {
                        ProductAttribute::create($attArray);
                    }
                }
            }


            if ($request->hasFile('image_file')) {
                $imgArray['product_id'] = $data['record']->id;
                $imgArray['status'] = 1;
                $images = $request->file('image_file');
                $image_titles = $request->input('image_title');

                if (count($images) > 0) {

                    for ($i = 0; $i < count($images); $i++) {
                        $name = uniqid() . '_' . $images[$i]->getClientOriginalName();
                        $images[$i]->move('assets/images/product', $name);

                        $imgArray['title'] = $image_titles[$i];
                        $imgArray['name'] = $name;


//                        ProductImage::update($imgArray);
                        ProductImage::create($imgArray);

                        $sizes = config('size.product');
                        $imager = Image::make('assets/images/product/' . $name);

                        foreach ($sizes as $size) {
                            $imager->resize($size['width'], $size['height']);
                            $imager->save('assets/images/product/' . $size['width'] . '_' . $size['height'] . '_' . $name);
                        }

                    }

                }
            }
                $request->session()->flash('success', $this->panel . ' updated successfully');
            } else {
                $request->session()->flash('error', $this->panel . ' update failed');
            }
            return redirect()->route($this->base_route . 'index');

        }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data['record'] = $this->model->findOrFail($id);
        if ($data['record']->delete()){
            request()->session()->flash('success',$this->panel . ' deleted successfully');
        } else {
            request()->session()->flash('error',$this->panel . ' delete failed');
        }
        return redirect()->route($this->base_route . 'index');
    }
    public function trash()
    {
        $data['records'] = $this->model->onlyTrashed()->get();
        return view($this->__loadDataToView($this->base_path . 'trash'),compact('data'));
    }

    public function restore($id)
    {
        $data['record'] = $this->model->onlyTrashed()->where('id',$id)->restore();
        request()->session()->flash('success',$this->panel . ' restored');

        return redirect()->route($this->base_route . 'trash');
    }

    public function forceDelete(Request  $request,$id)
    {
        $data['record'] = $this->model->onlyTrashed()->where('id',$id)->forceDelete();
        request()->session()->flash('success',$this->panel . ' deleted successfully');
        return redirect()->route($this->base_route . 'trash');
    }




}
