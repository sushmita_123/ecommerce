<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\ CategoryRequest;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class CategoryController extends BackendBaseController
{
    protected $panel = 'Category';
    protected  $base_path = 'backend.category.';
    protected  $base_route = 'backend.category.';

    public function  __construct(){
        $this->model = new Category();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['records'] = $this->model->all();
        return view($this->__loadDataToView($this->base_path . 'index'),compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view( $this->__loadDataToView($this->base_path . 'create'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        $request->request->add(['created_by' => Auth::user()->id]);

        if ($request->hasFile('image_file'))
        {
            $image = $request->file('image_file');
            //image storage
            $image_name = uniqid() . '_' . $image->getClientOriginalName();
            $image->move('assets/images/category',$image_name);
            $request->request->add(['image' => $image_name]);
            //image_resize
            $sizes = config('size.category');
            $imager = Image::make('assets/images/category/'.$image_name);

            foreach ($sizes as $size) {
                $imager->resize ($size['width'],$size['height']);
                $imager->save('assets/images/category/'. $size['width'] . '_' . $size['height'] . '_' .$image_name);
            }
        }
        $category = $this->model->create($request->all());
        if ($category){
            $request->session()->flash('success',$this->panel . ' created successfully');
        } else {
            $request->session()->flash('error',$this->panel . ' creation failed');
        }

        return redirect()->route($this->base_route . 'index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['record'] = $this->model->findOrFail($id);
        return  view($this->__loadDataToView($this->base_path . 'show'),compact('data'));
    }

    public function edit($id)
    {
        $data['record'] = $this->model->findOrFail($id);
        return  view($this->__loadDataToView($this->base_path . 'edit'),compact('data'));
    }

    public function update(CategoryRequest $request, $id)
    {
        $data['record'] = $this->model->findOrFail($id);

        $request->request->add(['updated_by' => Auth::user()->id]);


        if ($request->hasFile('image_file')) {
            $image = $request->file('image_file');
            //image storage
            $image_name = uniqid() . '_' . $image->getClientOriginalName();
            $image->move('assets/images/category', $image_name);
            $request->request->add(['image' => $image_name]);

            $sizes = config('size.category');

            //delete
            if ($data['record']->image && file_exists('assets/images/category/' . $data['record']->image)) {
                unlink('assets/images/category/' . $data['record']->image);

                foreach ($sizes as $size) {
                    if ($data['record']->image && file_exists('assets/images/category/' . $size['width'] . '_' . $size['height'] . '_'  . $data['record']->image)) {
                        unlink('assets/images/category/' . $size['width'] . '_' . $size['height'] . '_'  . $data['record']->image);
                    }
                }

            }

            $imager = Image::make('assets/images/category/' . $image_name);

            foreach ($sizes as $size) {
                $imager->resize($size['width'], $size['height']);
                $imager->save('assets/images/category/' . $size['width'] . '_' . $size['height'] . '_' . $image_name);

            }
        }

        if ($data['record']->update($request->all())) {
            $request->session()->flash('success', $this->panel . ' updated successfully');
        } else {
            $request->session()->flash('error', $this->panel . ' update failed');
        }

        return redirect()->route($this->base_route . 'index');

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data['record'] = $this->model->findOrFail($id);
        if ($data['record']->delete()){
            request()->session()->flash('success',$this->panel . ' deleted successfully');
        } else {
            request()->session()->flash('error',$this->panel . ' delete failed');
        }
        return redirect()->route($this->base_route . 'index');
    }
    public function trash()
    {
        $data['records'] = $this->model->onlyTrashed()->get();
        return view($this->__loadDataToView($this->base_path . 'trash'),compact('data'));
    }

    public function restore($id)
    {
        $data['record'] = $this->model->onlyTrashed()->where('id',$id)->restore();
        request()->session()->flash('success',$this->panel . ' restored');

        return redirect()->route($this->base_route . 'trash');
    }

    public function forceDelete(Request  $request,$id)
    {

        $data['record'] = $this->model->onlyTrashed()->where('id',$id)->forceDelete();
        $sizes = config('size.category');
        if ($data['record']->image && file_exists('assets/images/category/' . $data['record']->image)) {
            unlink('assets/images/category/' . $data['record']->image);

            foreach ($sizes as $size) {
                if ($data['record']->image && file_exists('assets/images/category/' . $size['width'] . '_' . $size['height'] . '_'  . $data['record']->image)) {
                    unlink('assets/images/category/' . $size['width'] . '_' . $size['height'] . '_'  . $data['record']->image);
                }
            }

        }
        request()->session()->flash('success',$this->panel . ' deleted successfully');
        return redirect()->route($this->base_route . 'trash');
    }

    public function getSubcategory(Request $request)
    {
        $category = Category::find($request->cid);
        $html = "<option value=''>Select Subcategory</option>";
        foreach ($category->subcategories as $subcategory) {
            $html .= "<option value='$subcategory->id'>$subcategory->name</option>";
        }
        return $html;
    }



}
