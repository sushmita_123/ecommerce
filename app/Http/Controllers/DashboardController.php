<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    function __construct(){
        $this->middleware('auth');
    }
    function index()
    {
        return view('backend.dashboard.index');
    }
    function  form(){
        return view('backend.dashboard.form');
    }

    function  table(){
        return view('backend.dashboard.table');
    }
    function  create(){
        return view('backend.dashboard.create');
    }
}
