<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;

class FrontendBaseController extends Controller
{
    protected  function  __loadDataToView($viewPath){
        view()->composer($viewPath, function ($view) {
            $categories = Category::where('status',1)->orderby('name')->get();
            $view->with('categories', $categories);
            $view->with('base_path', $this->base_path);
            $view->with('base_route', $this->base_route);
        });
        return $viewPath;
    }
}
