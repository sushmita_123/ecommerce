<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\Product;
use App\Models\Slider;
use Illuminate\Http\Request;

class FrontController extends FrontendBaseController
{
    protected  $base_path = 'frontend.front.';
    protected  $base_route = 'frontend.front.';

    public function index()
    {
        $data['sliders'] = Slider::all();
        $data['brands'] = Brand::orderby('rank')->get();
//        $data['flash_products'] = Product::where('flash_key',1)->get();
        $data['feature_products'] = Product::where('feature_key',1)->limit(8)->get();
        return view( $this->__loadDataToView($this->base_path . 'index'),compact('data'));
    }
}
