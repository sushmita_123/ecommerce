<?php

namespace App\Http\Middleware;

use App\Models\Permission;
use Closure;
use Illuminate\Http\Request;

class CheckPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        //get current user role
        $role = auth()->user()->role()->first();
        if ($role){
            //get current page route name
            $current_route = $request->route()->getName();
            //get id of current page route into permission
            $permission = Permission::where('route',$current_route)->first();
            if ($permission){
                //check assigned permission
                $ap = $role->permissions()->where('permission_id',$permission->id)->first();
                if (!$ap){
//                    abort(403);
                }
            } else {
//                abort(403);
            }
        } else {
//            abort(403);
        }
        return $next($request);
    }
}
