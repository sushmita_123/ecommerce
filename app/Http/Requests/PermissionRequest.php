<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PermissionRequest extends FormRequest
{

    public function authorize()
    {
    return true;
    }

    public function rules()
    {
        return [
            'name' => 'required',
            'route' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Please enter name',
            'route.required' => 'Please enter route'
        ];
    }
}
