<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BrandRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'rank' => 'required',
            'image_file' => (request()->method() == 'POST')?'required':'',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Please enter  name',
            'rank.required' => 'Please enter rank',
            'image_file.required'=> 'Please put image'
        ];
    }
}
