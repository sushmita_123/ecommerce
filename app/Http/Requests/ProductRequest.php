<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required',
            'slug' => 'required',
            'quantity'=> 'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Please enter name',
            'slug.required' => 'Please enter  slug',
            'quantity.required'=> 'quantity required'
        ];
    }
}
