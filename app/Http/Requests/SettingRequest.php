<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SettingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'logo' => 'required',
            'contact_number'=>'required',
            'email'=>'required',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Please enter title',
            'logo.required' => 'Please put logo',
            'contact_number.required'=>'please put contact details',
            'email.required'=>'please put your email address'
        ];
    }
}
