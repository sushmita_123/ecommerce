<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SubcategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required',
            'slug' => 'required',
            'short_description'=> 'required',
            'image_file' => (request()->method() == 'POST')?'required':'',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Please enter tag name',
            'slug.required' => 'Please enter tag slug',
            'short_description.required' => 'Please enter short_description',
            'image_file.required'=> 'Please put image'
        ];
    }
}
