<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use HasFactory;
    use HasFactory;
    use SoftDeletes;

    protected  $table = 'products';

    protected $fillable = ['category_id','subcategory_id','name','slug','code','stock','price','quantity','created_by','updated_by','short_description','description','meta_title','meta_description','meta_keyword','feature_key','flash_key'];
    public function createdBy()
    {
        return $this->belongsTo(User::class,'created_by','id');
    }

    public function updatedBy()
    {
        return $this->belongsTo(User::class,'updated_by','id');
    }
    public function category()
    {
        return $this->belongsTo(Category::class,'category_id','id');
    }
    public function subcategory()
    {
        return $this->belongsTo(SubCategory::class,'subcategory_id','id');
    }

    public function tags(){
        return $this->belongsToMany(Tag::class);
    }

    public function productAttributes(){
       return $this->hasMany(ProductAttribute::class,'product_id');
    }

    public function productImages(){
        return $this->hasMany(ProductImage::class,'product_id');
    }

}
