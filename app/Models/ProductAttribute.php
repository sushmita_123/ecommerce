<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductAttribute extends Model
{
    use HasFactory;

    protected $table = 'product_attribute';
    protected $fillable = ['value','attribute_id','product_id'];

    function attribute(){
        return $this->belongsTo(Attribute::class);
    }
}
