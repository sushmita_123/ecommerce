<div class="form-group">
    {!! Form::label('name', 'Name'); !!}
    {!! Form::text('name',null, ['class' => 'form-control']); !!}
    @include('backend.includes.validation_message',['field' => 'name'])
</div>
<div class="form-group">
    {!! Form::label('rank', 'Rank'); !!}
    {!! Form::number('rank',null, ['class' => 'form-control']); !!}
    @include('backend.includes.validation_message',['field' => 'rank'])
</div>
<div class="form-group">
    {!! Form::label('image', 'Image'); !!}
    {!! Form::file('image_file',null, ['class' => 'form-control']); !!}
    @include('backend.includes.validation_message',['field' => 'image_file'])
</div>
<div class="form-group">
    {!! Form::submit($button,['class' => 'btn btn-success']) !!}
    {!! Form::button('Reset',['type' => 'reset','class' => 'btn btn-danger']); !!}
</div>
