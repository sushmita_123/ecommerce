@extends('backend.layouts.backend')
@section('title','View $panel')
@section('main-content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{$panel}} Management</h1>
                    <a href="{{route($base_route . 'index')}}" class="btn btn-info">List</a>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                        <li class="breadcrumb-item" ><a href="{{route($base_route.'index')}}">Tag</a></li>
                        <li class="breadcrumb-item active">Details</li>

                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">View {{$panel}}</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                        <i class="fas fa-times"></i>
                    </button>
                </div>
            </div>
            <div class="card-body">
                @if(session('success'))
                    <div class="alert alert-success">{{session('success')}}</div>
                @endif
                @if(session('error'))
                    <div class="alert alert-danger">{{session('error')}}</div>
                @endif
                <table class="table table-bordered">
                    <tr>
                        <th>Title</th>
                        <td>{{$data['record']->title}}</td>
                    </tr>
                    <tr>
                        <th>Main_Heading</th>
                        <td>{{$data['record']->main_heading}}</td>
                    </tr>
                    <tr>
                        <th>Sub_Heading</th>
                        <td>{{$data['record']->sub_heading}}</td>
                    </tr>

                    <tr>
                        <th>Product ID</th>
                        <td>
                            @if($data['record']->product_id)
                                {{$data['record']->product->name}}
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>Image</th>
                        <td>
                            @if($data['record']->image && file_exists(public_path() . '/assets/images/slider/500_600_' .$data['record']->image ))
                                <img src="{{asset('assets/images/slider/500_600_' .$data['record']->image)}}" alt="{{$data['record']->name}}">
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>LInk</th>
                        <td>{{$data['record']->link}}</td>           </tr>
                    <tr>
                        <th>Created At</th>
                        <td>{{$data['record']->created_at}}</td>
                    </tr>
                    <tr>
                        <th>Updated At</th>
                        <td>{{$data['record']->updated_at}}</td>
                    </tr>
                </table>
            </div>
            <!-- /.card-body -->

            <!-- /.card-footer-->
        </div>
        <!-- /.card -->
    </section>
    <!-- /.content -->
@endsection
