<div class="form-group">
    {!! Form::label('product_id', 'Product'); !!}
    {!! Form::select('product_id',$data['products'],null, ['class' => 'form-control','placeholder' => 'Select Product']); !!}
    @include('backend.includes.validation_message',['field' => 'product_id'])
</div>
<div class="form-group">
    {!! Form::label('title', 'Title'); !!}
    {!! Form::text('title',null, ['class' => 'form-control']); !!}
    @include('backend.includes.validation_message',['field' => 'title'])
</div>
<div class="form-group">
    {!! Form::label('main_heading', 'Main_Heading'); !!}
    {!! Form::text('main_heading',null, ['class' => 'form-control']); !!}
    @include('backend.includes.validation_message',['field' => 'main_heading'])
</div>
<div class="form-group">
    {!! Form::label('sub_heading', 'Sub_heading'); !!}
    {!! Form::text('sub_heading',null, ['class' => 'form-control']); !!}
    @include('backend.includes.validation_message',['field' => 'sub_heading'])
</div>

<div class="form-group">
    {!! Form::label('link', 'Link'); !!}
    {!! Form::text('link',null, ['class' => 'form-control']); !!}
    @include('backend.includes.validation_message',['field' => 'link'])
</div>
<div class="form-group">
    {!! Form::label('image', 'Image'); !!}
    {!! Form::file('image_file',null, ['class' => 'form-control']); !!}
    @include('backend.includes.validation_message',['field' => 'image_file'])
</div>


<div class="form-group">
    {!! Form::submit($button,['class' => 'btn btn-success']) !!}
    {!! Form::button('Reset',['type' => 'reset','class' => 'btn btn-danger']); !!}
</div>
