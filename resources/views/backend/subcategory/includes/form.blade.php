<div class="form-group">
    {!! Form::label('category_id', 'Category'); !!}
    {!! Form::select('category_id',$data['categories'],null, ['class' => 'form-control','placeholder' => 'Select Category']); !!}
    @include('backend.includes.validation_message',['field' => 'category_id'])
</div>
<div class="form-group">
    {!! Form::label('name', 'Name'); !!}
    {!! Form::text('name',null, ['class' => 'form-control']); !!}
    @include('backend.includes.validation_message',['field' => 'name'])
</div>
<div class="form-group">
    {!! Form::label('slug', 'Slug'); !!}
    {!! Form::text('slug',null, ['class' => 'form-control']); !!}
    @include('backend.includes.validation_message',['field' => 'slug'])
</div>
<div class="form-group">
    {!! Form::label('rank', 'Rank'); !!}
    {!! Form::number('rank',null, ['class' => 'form-control']); !!}
    @include('backend.includes.validation_message',['field' => 'rank'])
</div>
<div class="form-group">
    {!! Form::label('status', 'Status'); !!}
    {!! Form::radio('status',1,true); !!} Active
    {!! Form::radio('status',0); !!} De-Active
</div>
<div class="form-group">
    {!! Form::label('short_description', 'short_description'); !!}
    {!! Form::text('short_description',null, ['class' => 'form-control']); !!}
    @include('backend.includes.validation_message',['field' => 'short_description'])
</div>
<div class="form-group">
    {!! Form::label('image', 'Image'); !!}
    {!! Form::file('image_file',null, ['class' => 'form-control']); !!}
    @include('backend.includes.validation_message',['field' => 'image_file'])
</div>


<div class="form-group">
    {!! Form::submit($button,['class' => 'btn btn-success']) !!}
    {!! Form::button('Reset',['type' => 'reset','class' => 'btn btn-danger']); !!}
</div>
