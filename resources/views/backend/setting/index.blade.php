@extends('backend.layouts.backend')
@section('title','List ' . $panel)
@section('main-content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{$panel}} Management</h1>
                    <a href="{{route($base_route . 'create')}}" class="btn btn-info">Create</a>
                    <a href="{{route($base_route . 'trash')}}" class="btn btn-danger">Trash</a>

                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                        <li class="breadcrumb-item" ><a href="{{route($base_route . 'index')}}">{{$panel}}</a></li>
                        <li class="breadcrumb-item active"><a href="{{route($base_route.'index')}}">List</a></li>

                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">List {{$panel}}</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                        <i class="fas fa-times"></i>
                    </button>
                </div>
            </div>
            <div class="card-body">
                @if(session('success'))
                    <div class="alert alert-success">{{session('success')}}</div>
                @endif
                @if(session('error'))
                    <div class="alert alert-danger">{{session('error')}}</div>
                @endif
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>SN</th>
                        <th>Title</th>
                        <th>Logo</th>
                        <th>Contact_number</th>
                        <th>Email</th>
                        <th>Created At</th>
                        <th>Action</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($data['records'] as $record)
                        <tr>
                            <td>{{$loop->index+1}}</td>
                            <td>{{$record->title}}</td>
                            <td>{{$record->logo}}</td>
                            <td>{{$record->contact_number}}</td>
                            <td>{{$record->email}}</td>
                            <td>{{$record->created_at}}</td>
                            <td>
                                <a href="{{route($base_route . 'show',$record->id)}}" class="btn btn-info">View</a>
                                <a href="{{route($base_route .'edit',$record->id)}}" class="btn btn-warning">Edit</a>
                                {!! Form::open(['route' => [$base_route .'destroy', $record->id],'method' => 'delete']) !!}
                                {!! Form::submit('Delete',['class' => 'btn btn-danger']) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                Footer
            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->
    </section>
    <!-- /.content -->
@endsection
