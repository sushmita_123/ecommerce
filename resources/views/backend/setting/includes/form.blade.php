<div class="form-group">
    {!! Form::label('title', 'Title'); !!}
    {!! Form::text('title',null, ['class' => 'form-control']); !!}
    @include('backend.includes.validation_message',['field' => 'title'])
</div>
<div class="form-group">
    {!! Form::label('logo', 'Logo'); !!}
    {!! Form::text('logo',null, ['class' => 'form-control']); !!}
    @include('backend.includes.validation_message',['field' => 'logo'])
</div>
<div class="form-group">
    {!! Form::label('contact_number', 'Contact_Number'); !!}
    {!! Form::number('contact_number',null, ['class' => 'form-control']); !!}
    @include('backend.includes.validation_message',['field' => 'contact_number'])
</div>
<div class="form-group">
    {!! Form::label('email', 'Email'); !!}
    {!! Form::text('email',null, ['class' => 'form-control']); !!}
    @include('backend.includes.validation_message',['field' => 'email'])
</div>


<div class="form-group">
    {!! Form::submit($button,['class' => 'btn btn-success']) !!}
    {!! Form::button('Reset',['type' => 'reset','class' => 'btn btn-danger']); !!}
</div>
