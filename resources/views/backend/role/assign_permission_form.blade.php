@extends('backend.layouts.backend')
@section('title','View $panel')
@section('main-content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{$panel}} Management</h1>
                    <a href="{{route($base_route . 'index')}}" class="btn btn-info">List</a>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                        <li class="breadcrumb-item" ><a href="{{route($base_route.'index')}}">Tag</a></li>
                        <li class="breadcrumb-item active">Details</li>

                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">View {{$panel}}</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                        <i class="fas fa-times"></i>
                    </button>
                </div>
            </div>
            <div class="card-body">
                @if(session('success'))
                    <div class="alert alert-success">{{session('success')}}</div>
                @endif
                @if(session('error'))
                    <div class="alert alert-danger">{{session('error')}}</div>
                @endif
                <table class="table table-bordered">
                    <tr>
                        <th>Name</th>
                        <td>{{$data['record']->name}}</td>
                    </tr>
                    <tr>
                        <th colspan="2" style="text-align: center">Permission List</th>
                    </tr>
                    {!! Form::open(['route' => [$base_route . 'assign_permission',$data['record']->id],'method' => 'post']) !!}

                @foreach($data['modules'] as $module)
                    <tr>
                        <td>{{$module->name}}</td>
                        <td>
                            <ul style="list-style: none">
                                @foreach($module->permissions as $permission)
                                    <li>
                                        @if(in_array($permission->id,$data['assigned_permission_list']))
                                        {!! Form::checkbox('permission_id[]',$permission->id,true) !!}  {{$permission->name}}
                                        @else
                                            {!! Form::checkbox('permission_id[]',$permission->id) !!}  {{$permission->name}}
                                        @endif
                                    </li>
                                @endforeach
                            </ul>
                        </td>
                    </tr>

                    @endforeach
                    <tr>
                        <td colspan="2">{!! Form::submit('Assign',['class' => 'btn btn-info']) !!}</td>
                    </tr>
                    {!! Form::close(); !!}
                </table>
            </div>
            <!-- /.card-body -->

            <!-- /.card-footer-->
        </div>
        <!-- /.card -->
    </section>
    <!-- /.content -->
@endsection
