@extends('backend.layouts.backend')
@section('title','View $panel')
@section('main-content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{$panel}} Management</h1>
                    <a href="{{route($base_route . 'index')}}" class="btn btn-info">List</a>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                        <li class="breadcrumb-item" ><a href="{{route($base_route.'index')}}">Product</a></li>
                        <li class="breadcrumb-item active">Details</li>

                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">View {{$panel}}</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                        <i class="fas fa-times"></i>
                    </button>
                </div>
            </div>
            <div class="card-body">
                @if(session('success'))
                    <div class="alert alert-success">{{session('success')}}</div>
                @endif
                @if(session('error'))
                    <div class="alert alert-danger">{{session('error')}}</div>
                @endif
                    <div class="card-body">
                        {!! Form::open(['route' => $base_route . 'store','method' => 'post', 'enctype' => 'multipart/form-data']) !!}
                        <div class="col-12 col-sm-12">
                            <div class="card card-primary card-outline card-outline-tabs">
                                <div class="card-header p-0 border-bottom-0">
                                    <ul class="nav nav-tabs" id="custom-tabs-four-tab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="custom-tabs-four-basic-tab" data-toggle="pill" href="#custom-tabs-four-basic" role="tab" aria-controls="custom-tabs-four-basic" aria-selected="true">Basic Information</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="custom-tabs-four-meta-tab" data-toggle="pill" href="#custom-tabs-four-meta" role="tab" aria-controls="custom-tabs-four-meta" aria-selected="false">Meta Information</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="custom-tabs-four-tag-tab" data-toggle="pill" href="#custom-tabs-four-tag" role="tab" aria-controls="custom-tabs-four-tag" aria-selected="false">Tags</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="custom-tabs-four-attribute-tab" data-toggle="pill" href="#custom-tabs-four-attribute" role="tab" aria-controls="custom-tabs-four-attribute" aria-selected="false">Attribute</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="custom-tabs-four-image-tab" data-toggle="pill" href="#custom-tabs-four-image" role="tab" aria-controls="custom-tabs-four-image" aria-selected="false">Image</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="card-body">
                                    <div class="tab-content" id="custom-tabs-four-tabContent">
                                        <div class="tab-pane fade active show" id="custom-tabs-four-basic" role="tabpanel" aria-labelledby="custom-tabs-four-basic-tab">


                <table class="table table-bordered">
                    <tr>
                        <th>Name</th>
                        <td>{{$data['record']->name}}</td>
                    </tr>
                    <tr>
                        <th>Slug</th>
                        <td>{{$data['record']->slug}}</td>
                    </tr>
                    <tr>
                        <th>Stock</th>
                        <td>{{$data['record']->stock}}</td>
                    </tr>
                    <tr>
                        <th>Quantity</th>
                        <td>{{$data['record']->quantity}}</td>
                    </tr>

                    <tr>
                        <th>Category ID</th>
                        <td>
                            @if($data['record']->category_id)
                                {{$data['record']->category->name}}
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>SubCategory ID</th>
                        <td>
                            @if($data['record']->subcategory_id)
                                {{$data['record']->subcategory->name}}
                            @endif
                        </td>
                    </tr>

                    <tr>
                    <th>Created At</th>
                    <td>{{$data['record']->created_at}}</td>
                    </tr>
                    <tr>
                        <th>Updated At</th>
                        <td>{{$data['record']->updated_at}}</td>
                    </tr>
                </table>
                                        </div>
                                        <div class="tab-pane fade" id="custom-tabs-four-meta" role="tabpanel" aria-labelledby="custom-tabs-four-meta-tab">
                                            <table class="table table-bordered">
                                                <tr>
                                                    <th>Meta-title</th>
                                                    <td>{{$data['record']->meta_title}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Meta-Description</th>
                                                    <td>{{$data['record']->meta_description}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Meta-Keyword</th>
                                                    <td>{{$data['record']->meta_keyword}}</td>
                                                </tr>
                                            </table>

                                        </div>
                                        <div class="tab-pane fade" id="custom-tabs-four-tag" role="tabpanel" aria-labelledby="custom-tabs-four-tag-tab">
                                            <table class="table table-bordered">
                                                <tr>
                                                    <th>Tag</th>
                                                    <td>
                                                        <ul>
                                                        @foreach($data['record']->tags as $tag)
                                                        <li>{{$tag->name}}</li>
                                                    @endforeach
                                                        </ul>
                                                    </td>
                                                </tr>

                                            </table>
                                        </div>

                                        <div class="tab-pane fade" id="custom-tabs-four-attribute" role="tabpanel" aria-labelledby="custom-tabs-four-attribute-tab">

                                            <table class="table table-bordered">
                                                <tr>
                                                    <th>Attribute</th>
                                                    <th>Value</th>
                                            </tr>
                                                @foreach($data['record']->productAttributes as $attribute)
                                                    <tr>
                                                        <td>{{$attribute->attribute->name}}</td>
                                                        <td>{{$attribute->value}}</td>
                                                    </tr>
                                                    @endforeach
                                            </table>

                                        </div>
                                        <div class="tab-pane fade" id="custom-tabs-four-image" role="tabpanel" aria-labelledby="custom-tabs-four-image-tab">
                                           <table class="table table-bordered">
{{--                                               <tr>--}}
{{--                                                   <th>name</th>--}}
{{--                                                   <td>{{$data['record']->name}}</td>--}}
{{--                                               </tr>--}}
                                               <tr>
                                                   <th>Title</th>
                                                   <td>{{$data['record']->title}}</td>
                                               </tr>
                                           </table>

                                        </div>


        </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
        <!-- /.card -->
    </section>
    <!-- /.content -->
@endsection
