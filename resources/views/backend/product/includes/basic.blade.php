<div class="form-group">
    {!! Form::label('category_id', 'Category'); !!}
    {!! Form::select('category_id',$data['categories'],null, ['class' => 'form-control','placeholder' => 'Select Category']); !!}
    @include('backend.includes.validation_message',['field' => 'category_id'])
</div>
<div class="form-group">
    {!! Form::label('subcategory_id', 'SubCategory'); !!}
    {!! Form::select('subcategory_id',$data['sub_categories'],null, ['class' => 'form-control','placeholder' => 'Select Subcategory']); !!}
    @include('backend.includes.validation_message',['field' => 'subcategory_id'])
</div>
<div class="form-group">
    {!! Form::label('name', 'Name'); !!}
    {!! Form::text('name',null, ['class' => 'form-control']); !!}
    @include('backend.includes.validation_message',['field' => 'name'])
</div>
<div class="form-group">
    {!! Form::label('slug', 'Slug'); !!}
    {!! Form::text('slug',null, ['class' => 'form-control']); !!}
    @include('backend.includes.validation_message',['field' => 'slug'])
</div>
<div class="form-group">
    {!! Form::label('code', 'Code'); !!}
    {!! Form::number('code',null, ['class' => 'form-control']); !!}
    @include('backend.includes.validation_message',['field' => 'code'])
</div>
<div class="form-group">
    {!! Form::label('quantity', 'Quantity'); !!}
    {!! Form::number('quantity',null, ['class' => 'form-control']); !!}
    @include('backend.includes.validation_message',['field' => 'quantity'])
</div>
<div class="form-group">
    {!! Form::label('price', 'Price'); !!}
    {!! Form::number('price',null, ['class' => 'form-control']); !!}
    @include('backend.includes.validation_message',['field' => 'price'])
</div>
<div class="form-group">
    {!! Form::label('short_description', 'Short Description'); !!}
    {!! Form::textarea('short_description',null, ['class' => 'form-control','rows' => 3]); !!}
    @include('backend.includes.validation_message',['field' => 'short_description'])
</div>
<div class="form-group">
    {!! Form::label('description', 'Description'); !!}
    {!! Form::textarea('description',null, ['class' => 'form-control','rows' => 3]); !!}
    @include('backend.includes.validation_message',['field' => 'description'])
</div>
<div class="form-group">
    {!! Form::label('flash_key', 'Flash product'); !!}
    {!! Form::radio('flash_key',1); !!} Yes
    {!! Form::radio('flash_key',0,true); !!} No
</div>
<div class="form-group">
    {!! Form::label('feature_key', 'Feature product'); !!}
    {!! Form::radio('feature_key',1); !!} Yes
    {!! Form::radio('feature_key',0,true); !!} No
</div>


