<div class="form-group">
    {!! Form::label('tag_id', 'Tag'); !!}
    {!! Form::select('tag_id[]',$data['tags'],$data['assigned_tag'], ['class' => 'form-control','placeholder' => 'Select tag','multiple' => 'multiple']); !!}
</div>
