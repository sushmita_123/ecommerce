@extends('backend.layouts.backend')
@section('main-content')

    <div class="row">
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary card-tabs">
                <div class="card-header p-0 pt-1">
                    <ul class="nav nav-tabs" id="custom-tabs-five-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" href="{{route('backend.basic_information.index')}}" role="tab" aria-controls="custom-tabs-five-overlay" aria-selected="true">Basic Information</a>
                            <div class="col-md-12">
                                <div class="card-body">
                                    {!! Form::open(['route' => $base_route . 'store','method' => 'post']) !!}
                                    @include($base_path . 'includes.form',['button' => 'Save ' . $panel])
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="custom-tabs-five-overlay-dark-tab" data-toggle="pill" href="#custom-tabs-five-overlay-dark" role="tab" aria-controls="custom-tabs-five-overlay-dark" aria-selected="false">Meta Information</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="custom-tabs-five-normal-tab" data-toggle="pill" href="#custom-tabs-five-normal" role="tab" aria-controls="custom-tabs-five-normal" aria-selected="false">Image</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="custom-tabs-five-normal-tab" data-toggle="pill" href="#custom-tabs-five-normal" role="tab" aria-controls="custom-tabs-five-normal" aria-selected="false">Tag</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="custom-tabs-five-normal-tab" data-toggle="pill" href="#custom-tabs-five-normal" role="tab" aria-controls="custom-tabs-five-normal" aria-selected="false">Attribute</a>
                        </li>
                    </ul>
                </div>

                <div class="card-body">

                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>
@endsection
