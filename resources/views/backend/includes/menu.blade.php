<!-- Sidebar Menu -->
<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        <li class="nav-item">
            <a href="{{route('home')}}" class="nav-link">
                <i class="nav-icon fas fa-tachometer-alt"></i>
                <p>
                    Dashboard
                </p>
            </a>
        </li>
        <li class="nav-header">Product Management</li>
        <li class="nav-item">
            <a href="{{route('backend.tag.index')}}" class="nav-link">
                <i class="fas fa-tags"></i>
                <p>
                    Tag
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('backend.category.index')}}" class="nav-link">
                <i class="fas fa-clipboard-list"></i>
                <p>
                    Category
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('backend.subcategory.index')}}" class="nav-link">
                <i class="nav-icon fas fa-list"></i>
                <p>
                    Subcategory
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('backend.product.index')}}" class="nav-link">
                <i class="fab fa-product-hunt"></i>
                <p>
                    Product
                </p>
            </a>
        </li>

        </li> <li class="nav-item">
            <a href="{{route('backend.permission.index')}}" class="nav-link">
                <i class="fab fa-product-hunt"></i>
                <p>
                    Permission
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('backend.slider.index')}}" class="nav-link">
                <i class="fab fa-product-hunt"></i>
                <p>
                   Slider
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('backend.role.index')}}" class="nav-link">
                <i class="fab fa-product-hunt"></i>
                <p>
                    Role
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('backend.attribute.index')}}" class="nav-link">
                <i class="fab fa-product-hunt"></i>
                <p>
                    Attribute
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('backend.module.index')}}" class="nav-link">
                <i class="fab fa-product-hunt"></i>
                <p>
                    Module
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('backend.setting.index')}}" class="nav-link">
                <i class="fab fa-product-hunt"></i>
                <p>
                    Setting
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('backend.brand.index')}}" class="nav-link">
                <i class="fab fa-product-hunt"></i>
                <p>
                    Brand
                </p>
            </a>
        </li>
        <li class="nav-header">Order Management</li>
        <li class="nav-item">
            <a href="{{route('home')}}" class="nav-link">
                <i class="nav-icon fas fa-users"></i>
                <p>
                    Customer
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('home')}}" class="nav-link">
                <i class="fas fa-cart-arrow-down"></i>
                <p>
                    Order
                </p>
            </a>
        </li>

        <li class="nav-header">User Management</li>
        <li class="nav-item">
            <a href="{{route('backend.user.index')}}" class="nav-link">
                <i class="nav-icon fas fa-users"></i>
                <p>
                    Users
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('home')}}" class="nav-link">
                <i class="nav-icon fas fa-user"></i>
                <p>
                    Profile
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('home')}}" class="nav-link">
                <i class="nav-icon fas fa-sign-out-alt"></i>
                <p>
                    Logout
                </p>
            </a>
        </li>
    </ul>
</nav>
<!-- /.sidebar-menu -->

