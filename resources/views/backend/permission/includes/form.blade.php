<div class="form-group">
    {!! Form::label('module_id', 'Module'); !!}
    {!! Form::select('module_id',$data['modules'],null, ['class' => 'form-control','placeholder' => 'Select Module']); !!}
    @include('backend.includes.validation_message',['field' => 'module_id'])
</div>
<div class="form-group">
    {!! Form::label('name', 'Name'); !!}
    {!! Form::text('name',null, ['class' => 'form-control']); !!}
    @include('backend.includes.validation_message',['field' => 'name'])
</div>
<div class="form-group">
    {!! Form::label('route', 'Route'); !!}
    {!! Form::text('route',null, ['class' => 'form-control']); !!}
    @include('backend.includes.validation_message',['field' => 'route'])
</div>
<div class="form-group">
    {!! Form::label('status', 'Status'); !!}
    {!! Form::radio('status',1,true); !!} Active
    {!! Form::radio('status',0); !!} De-Active
</div>
<div class="form-group">
    {!! Form::submit($button,['class' => 'btn btn-success']) !!}
    {!! Form::button('Reset',['type' => 'reset','class' => 'btn btn-danger']); !!}
</div>
